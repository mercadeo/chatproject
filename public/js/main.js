
var socket = io.connect("http://192.168.127.89:3000",{"forceNew": true});
socket.on('messages', function(data) {
  console.log(data);
  render(data);
});

function addMessage(e){
  var messageObject = {
    author: document.getElementById("user").value,
    title: document.getElementById("message").value
  }
  socket.emit('new-message', messageObject);
  document.getElementById("user").value = "";
  document.getElementById("message").value = "";
  return false;
}

function privateUserMessage(e){
  var destination = document.getElementById("privateUser").value;
  var message = document.getElementById("privateMessage").value;
  socket.emit('private message', destination, message);
  document.getElementById("privateMessage").value = "";
  return false;
}

function messageReader(message) {
  return(`<div>
    <strong>${message.author}</strong>:
    <em>${message.title}<em>
  </div>`);  
}

function render(data){
  var html;
  if(getClass(data) === "Array"){
     html = data.map(messageReader).join(" ");
  } else {
    html = messageReader(data);
  }
  //console.log(data.map(messageReader));
  document.getElementById('messages').insertAdjacentHTML('beforeend', html);
}

function getClass(obj) {
  if (typeof obj === "undefined")
    return "undefined";
  if (obj === null)
    return "null";
  return Object.prototype.toString.call(obj)
    .match(/^\[object\s(.*)\]$/)[1];
}

function registerUser(userName){
  socket.emit('init', userName);
  document.getElementById('users').innerHTML = `<li>${userName}</li>`;
}

