var express = require('express');
var app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

app.use(express.static('public'));

const port = 3000

app.get('/', function(req, res) {
  res.status(200).send("home request successfully!!");
});

var users = {};
var sockets = {};

io.on('connection', function(socket){
  console.log('connected to socket');
  socket.on('init', function(username) {
    users[username] = socket.id;    // Store a reference to your socket ID
    sockets[socket.id] = { username : username, socket : socket };  // Store a reference to your socket
    console.log("users", users);
  });
  // Private message is sent from client with username of person you want to 'private message'
  socket.on('private message', function(to, message) {
      // Lookup the socket of the user you want to private message, and send them your message
      console.log("message received", users[to], sockets[users[to]]);
      sockets[users[to]].socket.emit(
          'messages', 
          { 
              title : message, 
              author : sockets[socket.id].username 
          }
      );
      socket.emit(
          'messages', 
          { 
              title : message, 
              author : sockets[socket.id].username 
          }
      );
  });


  socket.emit('messages', messages);
  socket.on('new-message', function(data){
    //guardar mensaje en REDIS o en otra DB
    if (isEmpty(data.author) || isEmpty(data.title)) {
      console.log(data, "NULL VALUES");
    }else {
      messages.push(data);
      console.log("message added");
      console.log(data);
      //io.sockets.emit('messages', messages);
      io.sockets.emit('messages', data);
    }
    
  });
});


//arreglo de mensajes usado para prueba
var messages = [{
  id: 3,
  title: `mensaje de prueba ${Date.now()}`,
  author: "gerald morales"
}];

const requestHandler = (request, response) => {  
  console.log(request.url)
  response.end('Hello Node.js Server!!!')
}

server.listen(port, function(err) {  
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
})


function isEmpty(obj) {
  if (getClass(obj) === "String"){
    return true && obj.trim()  === "";
  } else {
    for(var prop in obj) {
      if(obj.hasOwnProperty(prop))
      return false;
    }
  }
  return true && JSON.stringify(obj) === JSON.stringify({});
}

function getClass(obj) {
  if (typeof obj === "undefined")
    return "undefined";
  if (obj === null)
    return "null";
  return Object.prototype.toString.call(obj)
    .match(/^\[object\s(.*)\]$/)[1];
}